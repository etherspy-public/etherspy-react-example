import React, { Component } from 'react';
import EtherSpy from 'etherspy-lib';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  componentDidMount() {
    this.initES();
  }

  initES = async () => {
    const es = new EtherSpy();
    es.valueTransferUpdate(data => console.log('1) valueTransferUpdate', data));
    es.tokenTransferUpdate(data => console.log('2) tokenTransferUpdate', data));
    const socketId = await es.connect();
    console.log('Connected to ES node!', socketId);

    // Bitance wallet
    es.subscribeValueTransfer('0xD551234Ae421e3BCBA99A0Da6d736074f22192FF', 'FromTo');

    // TRX Token
    es.subscribeTokenTransfer('0xf230b790E05390FC8295F4d3F60332c93BEd42e2', {from: '*', to: '*'});
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to etherspy-react-example</h1>
        </header>
        <span className="App-intro">
          <p>At <code>componentDidMount</code> we subscribed on:</p> 
          <p>
            1) From+To value transfer updates: <code>Binance wallet(0xD551234Ae421e3BCBA99A0Da6d736074f22192FF)</code>
          </p>
          <p>
            2) From+To token transfer updates: <code>TRX (Tronix, 0xf230b790E05390FC8295F4d3F60332c93BEd42e2)</code>
          </p>
        </span>
        <br/>
        <u>See logs in dev console please.</u>
      </div>
    );
  }
}

export default App;
